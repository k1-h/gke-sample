Create a new cluster on Google Kubernetes Engine (GKE):
* Cluster should have at least 2 nodes
* Allow connection to port 30080 in Kubernetes nodes
* Allow HTTP traffic to Kubernetes Nodes
* Make sure nodes IP address is `static` and not `ephemeral`
* Create a load balancer and define the cluster instance group as backend and point it to port 80 of nodes, make sure it has `static` IP as well
* Point the domain to load balancer address


Apply following:

Install Helm:

```
bash get-helm.sh
```

Install ingress controller:

```
helm upgrade --set controller.service.type=NodePort --set controller.service.nodePorts.http=30080 --set controller.service.nodePorts.https=30443 --set replicaCount=2 -i ingress stable/nginx-ingress
```

Install MySQL:
```
kubectl apply -f mysql.yaml
```

Install Wordpress:
```
kubectl apply -f wordpress.yaml
```

Install HAProxy:
```
export NODE_1=`gcloud --format="value(networkInterfaces[0].accessConfigs[0].natIP)" compute instances list | head -n 1`
export NODE_2=`gcloud --format="value(networkInterfaces[0].accessConfigs[0].natIP)" compute instances list | tail -n 1`
sed -i "s/NODE_1/$NODE_1/g" haproxy.yaml
sed -i "s/NODE_2/$NODE_2/g" haproxy.yaml
kubectl apply -f haproxy.yaml
```

Install `ab` to test auto scaling:
```
apt-get install apache2-utils
```

Run following:
```
ab -n 50000 -c 100 http://wordpress.k1h.ir/
```

Use following commands to watch wordpress pods are being scaled:
```
kubectl get pods -w
kubectl get hpa -w
```
